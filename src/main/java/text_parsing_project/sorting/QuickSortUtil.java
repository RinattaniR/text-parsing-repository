package text_parsing_project.sorting;

import java.util.List;


/**
 * Created by Rinat_Zamaldinov on Jun 5, 2018
 */
public class QuickSortUtil {
	
	public static List<Integer> increasingQuickSort(List<Integer> list){
		int listSize = list.size();
		if(listSize>1){
			int start = 0;
			int end = listSize - 1;
			return innerIncreaseSortMethod(list, start, end);
		}
		return list;
	}
	
	
	public static List<Integer> decreasingQuickSort(List<Integer> list){
		int listSize = list.size();
		if(listSize>1){
			int start = 0;
			int end = listSize-1;
			return innerDecreaseSortMethod(list, start, end);
		}
		return list;
	}
	
    private static List<Integer> innerIncreaseSortMethod(List<Integer> list,int start, int end) {
    	
		if (start >= end)
            return list;
        int i = start, j = end;
        int cursor = i - (i - j) / 2;
        while (i < j) {
            while (i < cursor && (list.get(i) <= list.get(cursor))) {
                i++;
            }
            while (j > cursor && (list.get(cursor) <= list.get(j))) {
                j--;
            }
            if (i < j) {
                int temp = list.get(i);
                list.set(i, list.get(j));
                list.set(j, temp);
                if (i == cursor)
                    cursor = j;
                else if (j == cursor)
                    cursor = i;
            }
        }
        innerIncreaseSortMethod(list,start, cursor);
        innerIncreaseSortMethod(list,cursor+1, end);
		return list;
	}
    
    private static List<Integer> innerDecreaseSortMethod(List<Integer> list,int start, int end) {
		if (start >= end)
            return list;
        int i = start, j = end;
        int cursor = i - (i - j) / 2;
        while (i < j) {
            while (i < cursor && (list.get(i) >= list.get(cursor))) {
                i++;
            }
            while (j > cursor && (list.get(cursor) >= list.get(j))) {
                j--;
            }
            if (i < j) {
                int temp = list.get(j);
                list.set(j, list.get(i));
                list.set(i, temp);
                if (i == cursor)
                    cursor = j;
                else if (j == cursor)
                    cursor = i;
            }
        }
        innerDecreaseSortMethod(list,start, cursor);
        innerDecreaseSortMethod(list,cursor+1, end);
		return list;
	}
}
