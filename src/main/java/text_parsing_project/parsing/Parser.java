package text_parsing_project.parsing;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.io.FileReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Rinat_Zamaldinov on Jun 5, 2018
 */
public class Parser {
	private static Logger logger = LoggerFactory.getLogger(Parser.class);
	
	public static List<String> parseFileToStringList(String filepath, String delimeter) throws IOException{
		List<String> list = new ArrayList<String>();
		logger.debug("filepath = " + filepath);
		FileReader inputStream = null;

         try {
        	 inputStream = new FileReader(filepath);

             int cont;
        	 String listElem ="";
             while ((cont = inputStream.read()) != -1) {

            	 String c = (char) cont +"";
            	 if(!c.equals(delimeter)){
            		 listElem = listElem +c;
            		 logger.debug("listElem = "+ listElem + "\n");
            	 }
            	 if(c.equals(delimeter)){
            		 list.add(listElem);
            		 listElem ="";
            	 }
             }    
             if((cont = inputStream.read()) == -1){
        		 list.add(listElem);
        		 logger.debug("list = "+list);
             }
             
         } finally {
             if (inputStream != null) {
                 inputStream.close();
                 return list;
             }
         }
		return null;
	}
	
	
	public static List<Integer> parseFileToIntegerList(String filepath, String delimeter) throws IOException{
		List<Integer> list = new LinkedList<Integer>();
		logger.debug("filepath = " + filepath);
		FileReader inputStream = null;

		try {
       	 	inputStream = new FileReader(filepath);

            int cont;
       	 	String buffer ="";
            while ((cont = inputStream.read()) != -1) {

           	 	String c = (char) cont +"";
           	 	if(!c.equals(delimeter)&&Character.isDigit((char) cont)){
           	 		buffer = buffer +c;
           	 		logger.debug("buffer = "+ buffer + "\n");
           	 	}
           	 	if(!c.equals(delimeter)&&!Character.isDigit((char) cont)){
           	 		buffer = "";
           	 		logger.debug("buffer = "+ buffer + "\n");
           	 	}
           	 	if(c.equals(delimeter)){
           	 		if(!buffer.isEmpty()){
           	 			list.add(Integer.parseInt(buffer));
           	 		}
           	 		buffer ="";
           	 	}
            }    
            if((cont = inputStream.read()) == -1){
       		 	list.add(Integer.parseInt(buffer));
       		 	logger.debug("list = "+list);
            }
            
        } finally {
            if (inputStream != null) {
                inputStream.close();
                logger.debug("list = "+list);
                return list;
            }
        }
		return null;
    }
}
