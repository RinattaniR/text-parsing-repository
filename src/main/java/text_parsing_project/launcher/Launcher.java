package text_parsing_project.launcher;

import java.io.IOException;
import java.util.List;

import text_parsing_project.parsing.Parser;
import text_parsing_project.sorting.QuickSortUtil;


public class Launcher {
	
	private static String filepath = System.getProperty("filepath");
	private static String delimeter = System.getProperty("delimeter");
	
    public static void main( String[] args ) throws IOException{
  	
    	List<Integer> list = Parser.parseFileToIntegerList(filepath, delimeter);
    	System.out.print("List after increasing sort = " + QuickSortUtil.increasingQuickSort(list)+ "\n");
    	System.out.print("List after decreasing sort = " + QuickSortUtil.decreasingQuickSort(list)+ "\n");
    }
}
